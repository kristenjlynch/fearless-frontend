import React, { useEffect, useState } from 'react';

function AttendConferenceForm(props) {
    const [formData, setFormData] = useState({
        conference: '',
        name: '',
        email: '',
    })
    const [formClasses, setFormClasses ] = useState("create-attendee-form")
    const [congratulatoryClasses, setCongratulatoryClasses ] = useState("alert alert-success mb-0 d-none")


    const handleFormChange = async (event) => {
        const value = event.target.value;
        const name = event.target.name;
        setFormData( {...formData, [name]: value} );
    }

    const [conferences, setConferences] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8000/api/conferences/";
        const response = await fetch (url);

        if (response.ok) {
            const data = await response.json();
            console.log(data);
            setConferences(data.conferences)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    // deal with submissions
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.conference = formData.conference;
        data.name = formData.name;
        data.email = formData.email;
        console.log(data);

        const attendeeUrl = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(attendeeUrl, fetchConfig);
        if (response.ok) {
            const newAttendee = await response.json();
            console.log(newAttendee);
            // setFormData({
            //     conference: '',
            //     name: '',
            //     email: '',
            // })
            setFormClasses("create-attendee-form d-none")
            setCongratulatoryClasses("alert alert-success mb-0")
        }
    }


// const [conferences, setConferences] = useConferences(props.conferences);


let spinnerClasses = 'd-flex justify-content-center mb-3';
let dropdownClasses = 'form-select d-none';
if (conferences.length > 0) {
  spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
  dropdownClasses = 'form-select';
}



return(
        <div className="row">
        <div className="col col-sm-auto">
        <img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg"/>
        </div>
        <div className="col">
        <div className="card shadow">
            <div className="card-body">
            <form onSubmit={handleSubmit} className={formClasses}>
                <h1 className="card-title">It's Conference Time!</h1>
                <p className="mb-3">
                Please choose which conference
                you'd like to attend.
                </p>
                <div className={spinnerClasses} id="loading-conference-spinner">
                <div className="spinner-grow text-secondary" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
                </div>
                <div className="mb-3">
                    <select onChange={handleFormChange} name="conference" required id="conference" className={dropdownClasses}>
                        <option value="">Choose a conference</option>
                        {conferences.map((conference) => {
                            return (
                                < option key={conference.href} value={conference.href}>
                                    {conference.name}
                                </option>
                            )
                        })}
                    </select>
                    </div>
                <p className="mb-3">
                Now, tell us about yourself.
                </p>
                <div className="row">
                <div className="col">
                    <div className="form-floating mb-3">
                    <input onChange={handleFormChange} required placeholder="Your full name" type="text" id="name" name="name" className="form-control"/>
                    <label htmlFor="name">Your full name</label>
                    </div>
                </div>
                <div className="col">
                    <div className="form-floating mb-3">
                    <input onChange={handleFormChange} required placeholder="Your email address" type="email" id="email" name="email" className="form-control"/>
                    <label htmlFor="email">Your email address</label>
                    </div>
                </div>
                </div>
                <button className="btn btn-lg btn-primary">I'm going!</button>
            </form>
            <div className={congratulatoryClasses} id="success-message">
                Congratulations! You're all signed up!
            </div>
            </div>
        </div>
        </div>
    </div>
    )
}

export default AttendConferenceForm;
