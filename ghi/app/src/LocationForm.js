import React, { useEffect, useState } from 'react';

function LocationForm(props) {

    // // Set the useState hook to store "name" in the component's state,
    // // with a default initial value of an empty string.
    // const [name, setName] = useState('');

    // // Create the handleNameChange method to take what the user inputs
    // // into the form and store it in the state's "name" variable.
    // const handleNameChange = (event) => {
    //     const value = event.target.value;
    //     setName(value);
    // }

    // // Set the useState hook to store "roomCount" in the component's state,
    // // with a default initial value of zero.
    // const [roomCount, setRoomCount] = useState(0);

    // // Create the handleRoomCountChange method to take what the user inputs
    // // into the form and store it in the state's "roomCount" variable.
    // const handleRoomCountChange = (event) => {
    //     const value = event.target.value;
    //     setRoomCount(value);
    // }

    // // Set the useState hook to store "city" in the component's state,
    // // with a default initial value of an empty string.
    // const [city, setCity] = useState('');

    // // Create the handleCityChange method to take what the user inputs
    // // into the form and store it in the state's "city" variable.
    // const handleCityChange = (event) => {
    //     const value = event.target.value;
    //     setCity(value);
    // }

    // // Set the useState hook to store "state" in the component's state,
    // // with a default initial value of an empty string.
    // const [state, setState] = useState('');

    // // Create the handleStateChange method to take what the user inputs
    // // into the form and store it in the state's "name" variable.
    // const handleStateChange = (event) => {
    //     const value = event.target.value;
    //     setState(value);
    // }

    // form data
    const [formData, setFormData] = useState({
        name: '',
        room_count: '',
        city: '',
        state: '',
    })

    const handleFormChange = async (event) => {
        const value = event.target.value;
        const name = event.target.name;
        setFormData( {...formData, [name]: value} );
    }

    // state selection data
    const [states, setStates] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/states/';
        const response = await fetch(url);

        if (response.ok) {
        const data = await response.json();
            setStates(data.states)

        // const selectTag = document.getElementById('state');
        // for (let state of data.states) {
        //     const option = document.createElement('option');
        //     option.value = state.abbreviation;
        //     option.innerHTML = state.name;
        //     selectTag.appendChild(option);
        // }
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        // const data = {};
        // data.room_count = formData.roomCount;
        // data.name = formData.name;
        // data.city = formData.city;
        // data.state = formData.state;
        console.log(formData);

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            const newLocation = await response.json();
            console.log(newLocation);
            setFormData({
                name: '',
                room_count: '',
                city: '',
                state: '',
            })
            // setName('');
            // setRoomCount('');
            // setCity('');
            // setState('');
        }
    }

    return(
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new location</h1>
                    <form onSubmit={handleSubmit} id="create-location-htmlForm">
                    <div className="htmlForm-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Name" required
                            type="text" name="name" id="name" value={formData.name}
                            className="form-control" />
                            <label htmlFor="name">Name</label>
                    </div>
                    <div className="htmlForm-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Room count" required
                            type="number" name="room_count" id="room_count" value={formData.room_count}
                            className="form-control"/>
                            <label htmlFor="room_count">Room count</label>
                    </div>
                    <div className="htmlForm-floating mb-3">
                        <input onChange={handleFormChange} placeholder="City" required
                            type="text" name="city" id="city" value={formData.city} className="form-control"/>
                            <label htmlFor="city">City</label>
                    </div>
                    <div className="mb-3">
                    <select onChange={handleFormChange} required name="state" id="state" value={formData.state}
                        className="form-select">
                        <option value="">Choose a state</option>
                        {states.map(state => {
                            return (
                            <option key={state.abbreviation} value={state.abbreviation}>
                                {state.name}
                            </option>
                            );
                        })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default LocationForm;



// import React from 'react';

// function LocationhtmlForm () {
//     return (
//         <div className="row">
//         <div className="offset-3 col-6">
//             <div className="shadow p-4 mt-4">
//                 <h1>Create a new location</h1>
//                 <form id="create-location-htmlForm">
//                 <div className="htmlForm-floating mb-3">
//                     <input placeholder="Name" required type="text" name="name" id="name" className="htmlForm-control"/>
//                     <label htmlFor="name">Name</label>
//                 </div>
//                 <div className="htmlForm-floating mb-3">
//                     <input placeholder="Room count" required type="number" name="room_count" id="room_count" className="htmlForm-control"/>
//                     <label htmlFor="room_count">Room count</label>
//                 </div>
//                 <div className="htmlForm-floating mb-3">
//                     <input placeholder="City" required type="text" name="city" id="city" className="htmlForm-control"/>
//                     <label htmlFor="city">City</label>
//                 </div>
//                 <div className="mb-3">
//                     <select required name="state" id="state" className="htmlForm-select">
//                     <option value="">Choose a state</option>
//                     </select>
//                 </div>
//                 <button className="btn btn-primary">Create</button>
//                 </form>
//             </div>
//             </div>
//         </div>
//         );
// }

// export default LocationhtmlForm;
