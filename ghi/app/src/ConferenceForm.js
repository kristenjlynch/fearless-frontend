import React, { useEffect, useState } from 'react';

function ConferenceForm(props) {
    // set up conference form data
    const [formData, setFormData] = useState({
        name: '',
        starts: '',
        ends: '',
        description: '',
        max_presentations: '',
        max_attendees: '',
        location: '',
    })

    const handleFormChange = async (event) => {
        const value = event.target.value;
        const name = event.target.name;
        setFormData( {...formData, [name]: value} );
    }

    // location selection data
    const [locations, setLocations] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8000/api/locations/";
        const response = await fetch (url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    // deal with submissions
    const handleSubmit = async (event) => {
        event.preventDefault();

        // const data = {};
        // data.name = formData.name;
        // data.starts = formData.starts;
        // data.ends = formData.ends;
        // data.description = formData.description;
        // data.max_presentations = formData.maxPresentations;
        // data.max_attendees = formData.maxAttendees;
        // data.location = formData.location;
        console.log(formData);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
            'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);
            setFormData({
                name: '',
                starts: '',
                ends: '',
                description: '',
                max_presentations: '',
                max_attendees: '',
                location: '',
            })
        }
    }

    // return form JSX
    return(
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" value={formData.name} className="form-control"/>
                <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="Starts" required type="date" name="starts" id="starts" value={formData.starts} className="form-control"/>
                <label htmlFor="starts">Starts</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="Ends" required type="date" name="ends" id="ends" value={formData.ends} className="form-control"/>
                <label htmlFor="ends">Ends</label>
            </div>
            <div className="mb-3">
                <label htmlFor="description">Description</label>
                <textarea onChange={handleFormChange} placeholder="Description" className="form-control" required type="text" name="description" id="description" value={formData.description} rows="3"></textarea>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleFormChange}  placeholder="Max Presentations" required type="number" name="max_presentations" id="max_presentations" value={formData.max_presentations} className="form-control"/>
                <label htmlFor="max_presentations">Max presentations</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handleFormChange} placeholder="Max Attendees" required type="number" name="max_attendees" id="max_attendees" value={formData.max_attendees} className="form-control"/>
                <label htmlFor="max_attendees">Max attendees</label>
            </div>
            <div className="mb-3">
                <select onChange={handleFormChange} required id="location" name="location" value={formData.location} className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                    return (
                        < option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                    )
                })}
                </select>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
    </div>
    )
}

export default ConferenceForm;
