window.addEventListener('DOMContentLoaded', async () => {
const url = 'http://localhost:8000/api/states/';

try {
    const response = await fetch(url);
    if (!response.ok) {
        console.log('bad response');
        return `
        <div class="alert alert-primary d-flex align-items-center" role="alert">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
                <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
            </svg>
            <div>
                An example alert with an icon
            </div>
        </div>
        `;
    } else {
        const url = 'http://localhost:8000/api/states/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log(data);

            // Get the select tag element by its id 'state'
            const selectTag = document.getElementById("state");

            // For each state in the states property of the data
            for (let state of data.states) {
               // Create an 'option' element
               const option = document.createElement("option");

                // Set the '.value' property of the option element to the
                // state's abbreviation
                option.value = state.abbreviation;

                // Set the '.innerHTML' property of the option element to
                // the state's name
                option.innerHTML = state.name;

                // Append the option element as a child of the select tag
                selectTag.appendChild(option);
            }
            const formTag = document.getElementById('create-location-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                const locationUrl = 'http://localhost:8000/api/locations/';
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    },
                };
                const response2 = await fetch(locationUrl, fetchConfig);
                if (response2.ok) {
                formTag.reset();
                const newLocation = await response2.json();
                }
            });
        }

    }

} catch (e) {
    console.log(e);
    return `
    <div class="alert alert-primary d-flex align-items-center" role="alert">
    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
        <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
    </svg>
    <div>
            There is an error: ${e};
    </div>
</div>
`;
    }
});
